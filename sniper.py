#!/usr/bin/env python

import argparse
import asyncio
import getpass
import re
import time

import discord
import requests
from requests_html import HTMLSession


# (guild_id, channel_id) tuples
SYD_SMASH_NEWS_CHANNEL = (101262762419720192, 120806780807675904)
STRUZ_TEST_CHANNEL = (393767861203501066, 393767861203501069)
SIGNUP_CHANNELS = [SYD_SMASH_NEWS_CHANNEL, STRUZ_TEST_CHANNEL]


# Use [^\0] to replace the '.' matcher with something that matches line breaks
sf_bracket_announcement_re = re.compile("[^\0]*(https://challonge[.]com/\S*)[^\0]*")
signup_re = re.compile(".*/tournaments/signup/.*")


# === Begin discord client definitions ===

# Set up our discord client hooks
client = discord.Client()


@client.event
async def on_ready():
    print('Logged in as:')
    print(client.user.name)
    print('User ID is:')
    print(client.user.id)
    print('------')


@client.event
async def on_message(message):
    process_message = False
    for channel_tuple in SIGNUP_CHANNELS:
        if message.channel.guild.id == channel_tuple[0] and message.channel.id == channel_tuple[1]:
            process_message = True

    if not process_message:
        return

    print('Got message:')
    print(message.content)

    match = sf_bracket_announcement_re.match(message.content)
    if match:
        challonge_url = match.groups()[1]

        print('bracket announced! signing up in 5 seconds')
        print('Signup link: %s' % challonge_url)
        time.sleep(5)
        bracket_signup(challonge_url, client._cmdargs.challonge_session)


# === End discord client definitions ===


def bracket_signup(tournament_url, login_token):
    session = HTMLSession()
    session.cookies.set_cookie(requests.cookies.create_cookie('_challonge_session_production', login_token))

    # If we've been provided a straight /tournaments/signup link, we don't need
    # to find the link.
    if signup_re.match(tournament_url):
        signup_page_link = tournament_url
    else:
        r = session.get(tournament_url)

        signup_page_link = ""
        for link in r.html.absolute_links:
            if signup_re.match(link):
                signup_page_link = link
                break

        if not signup_page_link:
            raise RuntimeError("Could not find signup link.")

    # Do the actual signup step
    r = session.get(signup_page_link)
    form = r.html.find(selector="form.signup", first=True)
    action_url = form.attrs["action"]

    # Use all the form data they supply us as part of the form submission
    form_data = {}
    for element in form.find(selector="input"):
        if "value" in element.attrs:
            form_data[element.attrs["name"]] = element.attrs["value"]
        else:
            form_data[element.attrs["name"]] = ""

    print("Signing up using link=%s and data:" % action_url)
    print(form_data)
    r = session.post("https://challonge.com" + action_url, data=form_data)
    print("Got response status_code: %s" % r.status_code)
    r.raise_for_status()
    print(r.html.find(selector=".alert", first=True).html)

    # if r.status_code != 200 then try signup again until retries done


def main():
    parser = argparse.ArgumentParser(description="Enter into a challonge bracket using an account's default name. Requires logging in to challonge in a browser and retrieving the value of the '_challonge_session_production' cookie.")
    parser.add_argument("--challonge-session", "-s", type=str, required=True,
                        help="Challonge session token from cookie '_challonge_session_production'")
    parser.add_argument("--discord-session", "-d", type=str,
                        help="Discord session token")
    args = parser.parse_args()

    if not args.discord_session:
        args.discord_session = getpass.getpass("Discord session token:")

    #bracket_signup(args.challonge_url, args.challonge_session)

    # Discord watcher
    client._cmdargs = args
    client.run(args.discord_session, bot=False)


if __name__ == "__main__":
    main()

# TODO: javascript to get the session token and user stuff
# timer / retries to signup
# test on signup closed until #time
#   - make search more resilient

# Stretch goals - track discord messages to get URL and then signup
# TODO: parse the difference between a signup link and a tournament link and
# skip the signup link finder if we get a tournament link. Honestly it'd probably even still work as-is now though.
# TODO: don't sign up if already signed up, scrape page for contextual information

# Instructions:
# get cookie from logging into challonge, F12 (dev tools) => Application => Cookies => copy the _challonge_session_production cookie value and use that as an argument
# get discord session token from logging into discord, F12 => Application => Local Storage => copy the value from "token"
